package com.ezmobdev.balachat.viewModels

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ezmobdev.balachat.activities.MainActivity
import com.ezmobdev.balachat.api.IChatRepo
import com.ezmobdev.balachat.model.MyMessage
import com.ezmobdev.balachat.model.User
import com.ezmobdev.balachat.repos.user.IUserRepo
import io.reactivex.disposables.CompositeDisposable

@SuppressLint("CheckResult")
class ChatViewModel(val api: IChatRepo, val repo: IUserRepo) : ViewModel() {
    val messagesData: MutableLiveData<ArrayList<MyMessage>> by lazy { MutableLiveData<ArrayList<MyMessage>>() }
    val userData: MutableLiveData<User> by lazy { MutableLiveData<User>() }
    private val compositeDisposable = CompositeDisposable()

    init {
        messagesData.value = arrayListOf()
        repo.loadUser()
            .map { userData.value = it }
            .subscribe()

        val f2 = api.observeNew().subscribe { data ->
            Log.d(MainActivity.TAG, "onNext $data")
            data.dt = System.currentTimeMillis()
            val liost = messagesData.value
            liost?.add(data)
            messagesData.postValue(liost)
            Log.d("ChatViewModelTAG", messagesData.value?.size.toString())
            //            activity?.runOnUiThread {
            //                messages.add(data)
            //                messages.sortWith(compareByDescending { it.dt })
            //                adapter.setData(messages)
            //            }


        }
        compositeDisposable.addAll(f2)

    }


    fun sendMessage(m: String) {
        repo.loadUser().subscribe {
            api.sendMessage(m, it.name)
        }

    }
}