package com.ezmobdev.balachat.viewModels

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ezmobdev.balachat.model.User
import com.ezmobdev.balachat.repos.user.IUserRepo


@SuppressLint("CheckResult")
class LoginViewModel(val userRepo: IUserRepo) : ViewModel() {

    val userData: MutableLiveData<User> by lazy { MutableLiveData<User>() }

    fun setUserName(name: String) {
        val user = User(name)
        userRepo.saveUser(user).subscribe {
            userData.value = user
        }

    }

}
