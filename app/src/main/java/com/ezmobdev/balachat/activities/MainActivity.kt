package com.ezmobdev.balachat.activities

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation.findNavController
import com.ezmobdev.balachat.R
import com.ezmobdev.balachat.fragments.OnFragmentListener
import com.ezmobdev.balachat.model.MyMessage


class MainActivity : FragmentActivity(), OnFragmentListener {


    companion object {
        val CHANNEL_ID = "001"
        const val TAG = "MainActivityTAG"
        const val URL = "ws://92.53.104.10:8080/ws"
        const val NOTIFY_ID = 1
//        const val URL = "ws://127.0.0.1:8080/ws"
    }

    lateinit var nm: NotificationManager

    override fun showNotification(msg: MyMessage) {
        Log.d(TAG, "showNotification $msg")
        val intent = Intent(applicationContext, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        val pi = PendingIntent.getActivity(applicationContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        val nb = NotificationCompat.Builder(applicationContext, CHANNEL_ID)
            .setAutoCancel(true)
            .setSmallIcon(R.drawable.notification_icon_background)
            .setWhen(System.currentTimeMillis())
            .setContentIntent(pi)
            .setContentTitle(msg.senderName)
            .setContentText(msg.msg)
            .setPriority(1)
        createChanel()
        nm.notify(NOTIFY_ID, nb.build())
    }

    override fun onError(e: String) {
        Toast.makeText(this, e, Toast.LENGTH_SHORT).show()
    }

    override fun onNextFragment(id: Int) {
        Log.d(TAG, "onNextFragment id = $id")
        nav.navigate(id)
    }


    lateinit var nav: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        nm = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        nav = findNavController(this, R.id.nav_host_fragment)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_settings -> onSettingsSelected()
            R.id.menu_login -> nav.navigate(R.id.chatFragment)
            R.id.menu_exit -> finish()

        }
        return super.onOptionsItemSelected(item)

    }

    private fun onSettingsSelected() {
        Toast.makeText(this, "настройки", Toast.LENGTH_SHORT).show()
    }


    private fun createChanel() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            nm.createNotificationChannel(
                NotificationChannel(
                    CHANNEL_ID,
                    CHANNEL_ID,
                    NotificationManager.IMPORTANCE_DEFAULT
                )
            )
        }
    }


}
