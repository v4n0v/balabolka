package com.ezmobdev.balachat.di.koin

import com.ezmobdev.balachat.activities.MainActivity
import com.ezmobdev.balachat.api.ChatRepo
import com.ezmobdev.balachat.api.ChatService
import com.ezmobdev.balachat.api.IChatRepo
import com.ezmobdev.balachat.repos.user.IUserRepo
import com.ezmobdev.balachat.repos.user.PaperUserRepo
import com.ezmobdev.balachat.viewModels.ChatViewModel
import com.ezmobdev.balachat.viewModels.LoginViewModel
import com.google.gson.Gson
import com.tinder.scarlet.Protocol
import com.tinder.scarlet.Scarlet
import com.tinder.scarlet.messageadapter.gson.GsonMessageAdapter
import com.tinder.scarlet.streamadapter.rxjava2.RxJava2StreamAdapterFactory
import com.tinder.scarlet.websocket.ShutdownReason
import com.tinder.scarlet.websocket.okhttp.OkHttpWebSocket
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module


val loginVewModelModule = module {
    single<IUserRepo> { PaperUserRepo() }
    viewModel {
        LoginViewModel(get())
    }

}


val apiModule = module {
    single {
        OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .build()
    }


    single {
        Gson()
    }

    single<Protocol> {
        OkHttpWebSocket(
            get(),
            OkHttpWebSocket.SimpleRequestFactory(
                { Request.Builder().url(MainActivity.URL).build() },
                { ShutdownReason.GRACEFUL }
            ))
    }

    single<Scarlet.Configuration> {
        Scarlet.Configuration(
            messageAdapterFactories = listOf(GsonMessageAdapter.Factory()),
            streamAdapterFactories = listOf(RxJava2StreamAdapterFactory())
//            lifecycle = AndroidLifecycle.ofApplicationForeground(App.instance)
        )
    }
    single<ChatService> {
        Scarlet(get(), get()).create()

    }

    single<IChatRepo> {
        ChatRepo(get(), get())
    }


    viewModel {
        ChatViewModel(get(), get())
    }

}