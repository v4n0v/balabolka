package com.ezmobdev.balachat

import android.app.Application
import com.ezmobdev.balachat.di.koin.apiModule
import com.ezmobdev.balachat.di.koin.loginVewModelModule
import io.paperdb.Paper
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class App : Application() {
    companion object {
        lateinit var instance: App
    }

    //    private lateinit var appComponent: AppComponent
//    fun getAppComponent(): AppComponent {
//        return appComponent
//    }
//    fun getVMComponent(): ViewModelSubComponent {
//        return viewModelSubComponent
//    }
//    private lateinit var viewModelSubComponent: ViewModelSubComponent
    override fun onCreate() {
        super.onCreate()
        instance = this
        Paper.init(this)

        startKoin {
            androidLogger()
            androidContext(this@App)
            modules(loginVewModelModule, apiModule)
        }

//        appComponent = DaggerAppComponent.builder()
//            .application(this)
//            .build()
////
//        viewModelSubComponent = appComponent.viewModelSubComponentBuilder().build()

    }


}