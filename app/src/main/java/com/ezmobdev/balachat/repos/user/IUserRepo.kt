package com.ezmobdev.balachat.repos.user

import com.ezmobdev.balachat.model.User
import io.reactivex.Observable
import io.reactivex.Observer

interface IUserRepo{
 fun saveUser(user: User):Observable<Boolean>
 fun loadUser(): Observable<User>

}