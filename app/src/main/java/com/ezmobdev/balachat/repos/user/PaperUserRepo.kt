package com.ezmobdev.balachat.repos.user

import com.ezmobdev.balachat.model.User
import io.paperdb.Paper
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.internal.operators.observable.ObservableCreate
import io.reactivex.schedulers.Schedulers

class PaperUserRepo : IUserRepo {
    companion object {
        const val USER_BOOK = "user_book"
        const val USER_KEY = "user_key"
    }

    override fun saveUser(user: User): Observable<Boolean> {
        return ObservableCreate<Boolean> {
            Paper.book(USER_BOOK).write(USER_KEY, user)
            it.onNext(true)
            it.onComplete()
        }.subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun loadUser(): Observable<User> {
        return ObservableCreate<User> {
            val user = Paper.book(USER_BOOK).read<User?>(USER_KEY)
            if (user != null)
                it.onNext(user)
            it.onComplete()
        }.subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
    }

}