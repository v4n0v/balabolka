package com.ezmobdev.balachat.fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.ezmobdev.balachat.R
import com.ezmobdev.balachat.databinding.FragmentNameBinding
import com.ezmobdev.balachat.viewModels.LoginViewModel
import org.koin.android.ext.android.inject

class LoginFragment : Fragment() {

    private var listener: OnFragmentListener? = null
    val viewModel: LoginViewModel by inject()
    private var binding: FragmentNameBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentNameBinding.inflate(
            inflater, container, false
        )
        viewModel.userData.observe(this,
            Observer {
                listener?.onNextFragment(R.id.chatFragment)
            })
        binding?.viewModel = viewModel
        return binding?.root
//        return inflater.inflate(R.layout.fragment_name, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentListener")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding?.btnNext?.setOnClickListener {
            Log.d("LoginFragmentTAG", "setOnClickListener")
            val name = binding?.etName?.text.toString()
            viewModel.setUserName(name)
        }

//        btnNext.setOnClickListener { listener?.onNextFragment(R.id.chatFragment) }

        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

}
