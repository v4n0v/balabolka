package com.ezmobdev.balachat.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ezmobdev.balachat.R
import com.ezmobdev.balachat.activities.MainActivity
import com.ezmobdev.balachat.custom.RecyclerAdapter
import com.ezmobdev.balachat.databinding.FragmentChatBinding
import com.ezmobdev.balachat.databinding.ItemMessageBinding
import com.ezmobdev.balachat.model.MyMessage
import com.ezmobdev.balachat.viewModels.ChatViewModel
import kotlinx.android.synthetic.main.fragment_chat.*
import org.koin.android.ext.android.inject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

@SuppressLint("CheckResult")
class ChatFragment : Fragment() {

    private var listener: OnFragmentListener? = null


    lateinit var adapter: RecyclerAdapter<MyMessage, ItemMessageBinding>
    private var binding: FragmentChatBinding? = null


    val viewModel: ChatViewModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentChatBinding.inflate(
            inflater, container, false
        )

        adapter = RecyclerAdapter.RecyclerAdapterBuilder<MyMessage, ItemMessageBinding>()
            .setLauoutId(R.layout.item_message)
            .initBinding { item, _ ->
                val isMe = (item.senderName == viewModel.userData.value?.name)
                val ll = ConstraintLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
                if (isMe) {
                    ll.endToEnd = this.constraintRoot.id
                    llContainer.background =
                        ContextCompat.getDrawable(listener as MainActivity, R.drawable.rounded_rectangle_main)

                } else {
                    ll.startToStart = this.constraintRoot.id
                    llContainer.background =
                        ContextCompat.getDrawable(listener as MainActivity, R.drawable.rounded_rectangle_accent)
                }

                llContainer.layoutParams = ll

                this.tvSender.text = if (isMe) "Вы" else item.senderName.trim()
                this.tvMessage.text = item.msg.trim()
                this.tvDt.text = SimpleDateFormat("HH:mm", Locale.getDefault()).format(Date(item.dt))
            }

            .build()

        val llm = LinearLayoutManager(listener as Context)
        llm.orientation = RecyclerView.VERTICAL
        llm.reverseLayout = true
        binding?.rvMesaages?.layoutManager = llm
        binding?.rvMesaages?.adapter = adapter

        binding?.viewModel = viewModel
        return binding?.root
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentListener")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        button.setOnClickListener {
            val t = inputText.text.toString()
            if (t.isNotEmpty())
                viewModel.sendMessage(t)
        }

        viewModel.messagesData.observe(this,
            androidx.lifecycle.Observer<ArrayList<MyMessage>> { messages ->
                messages?.let {
                    Log.d("ChatFragTAG", "observed ${it.size}")
                    it.sortWith(compareByDescending { it.dt })
                    adapter.setData(it)
                    if (it.isNotEmpty() && it.first().senderName != viewModel.userData.value?.name)
                        listener?.showNotification(it.first())
                }
            })

        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }


}
