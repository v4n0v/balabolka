package com.ezmobdev.balachat.fragments

import com.ezmobdev.balachat.model.MyMessage

interface OnFragmentListener {
    fun onNextFragment(id: Int)
    fun onError(e: String)
    fun showNotification(msg:MyMessage)
}