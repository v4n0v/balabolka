package com.ezmobdev.balachat.model

import java.io.Serializable

data class MyMessage(
                     val msg:String,
                     val senderName: String,
                     var dt: Long = System.currentTimeMillis()) :Serializable{
    override fun toString(): String {
        return "$senderName: $msg"
    }
}