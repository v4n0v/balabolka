package com.ezmobdev.balachat.api

import android.util.Log
import com.ezmobdev.balachat.activities.MainActivity
import com.ezmobdev.balachat.model.MyMessage
import com.google.gson.Gson
import com.tinder.scarlet.Scarlet
import com.tinder.scarlet.Stream
import com.tinder.scarlet.ws.Receive
import com.tinder.scarlet.ws.Send
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

interface ChatService {
    @Send
    fun sendText(message: MyMessage)

    @Receive
    fun observeText(): Flowable<MyMessage>
}

interface IChatRepo {
    fun observeNew(): Flowable<MyMessage>
    fun sendMessage(s: String, sender: String)
}

class ChatRepo(val client: ChatService, val gson: Gson) : IChatRepo {


    override fun observeNew(): Flowable<MyMessage> {
        return client.observeText().subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun sendMessage(s: String, sender: String) {
        Completable.fromAction {
            Log.d(MainActivity.TAG, "try Send")
            val msg = MyMessage(s, sender)
            val json = gson.toJson(msg)
            Log.d(MainActivity.TAG, json)
            client.sendText(msg)
        }.subscribeOn(Schedulers.computation())
            .doOnError {
                Log.d(MainActivity.TAG, "onError ${it.message}")
            }
            .subscribe()
    }

}